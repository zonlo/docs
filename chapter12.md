# 服务部署

## 独立部署

 ##### **说明：mpush 服务只依赖于zookeeper和redis，当然还有JDK>=1.8**

1. 安装`jdk 1.8` 以上版本并设置`%JAVA_HOME％`

2. 安装`zookeeper` (安装配置步骤略)

3. 安装`Redis` (安装配置步骤略)

4. 下载`mpush server`最新的正式包[https://github.com/mpusher/mpush/releases](https://github.com/mpusher/mpush/releases)

5. 解压下载的tar包`tar -zvxf mpush-release-x.x.x.tar.gz`到 mpush 目录, 结构如下

   ><pre>
   >drwxrwxr-x 2 shinemo shinemo  4096 Aug 20 09:30 bin —> 启动脚本
   >drwxrwxr-x 2 shinemo shinemo  4096 Aug 20 09:52 conf —> 配置文件
   >drwxrwxr-x 2 shinemo shinemo  4096 Aug 20 09:29 lib —> 核心类库
   >-rw-rw-r-- 1 shinemo shinemo 11357 May 31 11:07 LICENSE
   >drwxrwxr-x 2 shinemo shinemo  4096 Aug 20 09:32 logs —> 日志目录
   >-rw-rw-r-- 1 shinemo shinemo    21 May 31 11:07 README.md
   >drwxrwxr-x 2 shinemo shinemo  4096 Aug 20 09:52 tmp
   ></pre>

6. 修改 conf 目录下的 `vi mpush.conf`文件, `mpush.conf`里的配置项会覆盖同目录下的`reference.conf`文件
   ```java
   //主要修改以下配置
   mp.net.connect-server-port=3000//长链接服务对外端口, 公网端口
   mp.zk.server-address="127.0.0.1:2181"//zk 机器的地址
   mp.redis={//redis 相关配置
     #redis 集群配置，group 是个二维数组，第一层表示有多少组集群，每个集群下面可以有多台机器
     cluster-group:[["127.0.0.1:6379"]]//格式ip:port:password,密码可以不设置ip:port
   }
   //还有用于安全加密的RSA mp.security.private-key 和 mp.security.public-key 等...
   ```
    如果要修改其他配置请参照`reference.conf`文件

7. 给bin目录下的脚本增加执行权限`chmod u+x *.sh`

8. 执行`./mp.sh start` 启动服务, 查看帮助`./mp.sh` 目前支持的命令：

  >`Usage: ./mp.sh {start|start-foreground|stop|restart|status|upgrade|print-cmd}`
  >
  >`set-env.sh`用于增加和修改jvm启动参数，比如堆内存、开启远程调试端口、开启jmx等

9. ```cd logs```目录，```cat mpush.out```查看服务是否启动成功


## 集成部署

1. 添加Maven依赖到工程

   ```xml
   <dependency>
     <groupId>com.github.mpusher</groupId>
     <artifactId>mpush-boot</artifactId>
     <version>0.0.2</version>
   </dependency>
   ```

   ​

2. 启动入口类 `com.mpush.bootstrap.ServerLauncher.java`
   >启动调用 start 方法，停止调用 stop 方法

3. 在工程里添加classpath下添加`application.conf`配置文件，配置方式参照独立部署第6点

4. spring bean 方式配置

   ```xml
   <bean class="com.mpush.bootstrap.ServerLauncher" init-method="start" destroy-method="stop"/>
   ```

